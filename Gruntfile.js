module.exports = function(grunt) {


    grunt.initConfig({


        package: grunt.file.readJSON('package.json'),

        paths: {
            source: 'source/',
            distribution: 'distribution/'
        },

        sass: { // Task
            dist: { // Target
                options: { // Target options
                    style: 'expanded'
                },
                files: { // Dictionary of files
                    '<%= paths.distribution %>stylesheets/style.css': ['<%= paths.source %>stylesheets/style.scss']
                }
            }
        },

        connect: {
            server: {
                options: {
                    port: 3000,
                    base: 'distribution'
                }
            }
        },

        copy: {
            main: {
                files: [{
                    expand: true,
                    cwd: '<%= paths.source %>',
                    src: [
                        'images/{,**/}*',
                        'fonts/{,**/}*',
                        'javascripts/{,**/}*',
                        'libraries/{,**/}*',
                        '*.ico',
                        '*.html'
                    ],
                    dest: '<%= paths.distribution %>'
                }]
            }
        },

        watch: {

            sass: {
                options: {
                    livereload: true
                },
                files: '<%= paths.source %>stylesheets/{,**/}*.scss',
                tasks: ['sass:dist']
            },

            html: {
                options: {
                    livereload: true
                },
                files: '<%= paths.source %>{,**/}*.html',
                tasks: ['copy:main']
            }

        }


    });


    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-connect');
    grunt.loadNpmTasks('grunt-html-validation');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-concat');


    grunt.registerTask('default', ['copy', 'connect', 'watch']);

};
